/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.gadgets.servlet;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.shindig.gadgets.Gadget;
import org.apache.shindig.gadgets.GadgetContext;
import org.apache.shindig.gadgets.UrlGenerator;
import org.apache.shindig.gadgets.process.ProcessingException;
import org.apache.shindig.gadgets.process.Processor;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.apache.shindig.gadgets.spec.LinkSpec;
import org.apache.shindig.gadgets.spec.ModulePrefs;
import org.apache.shindig.gadgets.spec.UserPref;
import org.apache.shindig.gadgets.spec.View;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Set;

/**
 * Processes JSON-RPC requests by retrieving all necessary meta data in parallel and coalescing into
 * a single output JSON construct.
 */
public class JsonRpcHandler {
  private final Processor processor;
  private final UrlGenerator urlGenerator;

  @Inject
  public JsonRpcHandler(Processor processor, UrlGenerator urlGenerator) {
    this.processor = processor;
    this.urlGenerator = urlGenerator;
  }

  /**
   * Processes a JSON request.
   *
   * @param request Original JSON request
   * @return The JSON response.
   */
  public JSONObject process(JSONObject request) throws RpcException, JSONException {
    List<GadgetContext> gadgets;

    JSONObject requestContext = request.getJSONObject("context");
    JSONArray requestedGadgets = request.getJSONArray("gadgets");

    // Process all JSON first so that we don't wind up with hanging threads if
    // a JSONException is thrown.
    gadgets = Lists.newArrayListWithExpectedSize(requestedGadgets.length());
    for (int i = 0, j = requestedGadgets.length(); i < j; ++i) {
      GadgetContext context = new JsonRpcGadgetContext(
              requestContext, requestedGadgets.getJSONObject(i));
      gadgets.add(context);
    }

    JSONObject response = new JSONObject();
    for (GadgetContext context : gadgets) {
      try {
        JSONObject gadget = getGadgetJson(context);
        response.append("gadgets", gadget);
      } catch (JSONException e) {
        throw new RpcException("Unable to write JSON", e);
      }
    }

    return response;
  }

  public JSONObject getGadgetJson(GadgetContext context) throws RpcException {
    try {
      Gadget gadget = processor.process(context);
      GadgetSpec spec = gadget.getSpec();

      JSONObject gadgetJson = new JSONObject();

      ModulePrefs prefs = spec.getModulePrefs();

      // TODO: modularize response fields based on requested items.
      JSONObject views = new JSONObject();
      for (View view : spec.getViews().values()) {
        views.put(view.getName(), new JSONObject()
                .put("type", view.getType().toString().toLowerCase())
                .put("quirks", view.getQuirks())
                .put("preferredHeight", view.getPreferredHeight())
                .put("preferredWidth", view.getPreferredWidth()));
      }

      // Features.
      Set<String> feats = prefs.getFeatures().keySet();
      String[] features = feats.toArray(new String[feats.size()]);

      // Links
      JSONObject links = new JSONObject();
      for (LinkSpec link : prefs.getLinks().values()) {
        links.put(link.getRel(), link.getHref());
      }

      JSONObject userPrefs = new JSONObject();

      // User pref specs
      for (UserPref pref : spec.getUserPrefs()) {
        JSONObject up = new JSONObject()
                .put("displayName", pref.getDisplayName())
                .put("type", pref.getDataType().toString().toLowerCase())
                .put("default", pref.getDefaultValue())
                .put("enumValues", pref.getEnumValues())
                .put("orderedEnumValues", getOrderedEnums(pref));
        userPrefs.put(pref.getName(), up);
      }

      // TODO: This should probably just copy all data from
      // ModulePrefs.getAttributes(), but names have to be converted to
      // camel case.
      gadgetJson.put("iframeUrl", urlGenerator.getIframeUrl(gadget))
              .put("url", context.getUrl().toString())
              .put("moduleId", context.getModuleId())
              .put("title", prefs.getTitle())
              .put("titleUrl", prefs.getTitleUrl().toString())
              .put("views", views)
              .put("features", features)
              .put("userPrefs", userPrefs)
              .put("links", links)

              // extended meta data
              .put("directoryTitle", prefs.getDirectoryTitle())
              .put("thumbnail", prefs.getThumbnail().toString())
              .put("screenshot", prefs.getScreenshot().toString())
              .put("author", prefs.getAuthor())
              .put("authorEmail", prefs.getAuthorEmail())
              .put("authorAffiliation", prefs.getAuthorAffiliation())
              .put("authorLocation", prefs.getAuthorLocation())
              .put("authorPhoto", prefs.getAuthorPhoto())
              .put("authorAboutme", prefs.getAuthorAboutme())
              .put("authorQuote", prefs.getAuthorQuote())
              .put("authorLink", prefs.getAuthorLink())
              .put("categories", prefs.getCategories())
              .put("screenshot", prefs.getScreenshot().toString())
              .put("height", prefs.getHeight())
              .put("width", prefs.getWidth())
              .put("showStats", prefs.getShowStats())
              .put("showInDirectory", prefs.getShowInDirectory())
              .put("singleton", prefs.getSingleton())
              .put("scaling", prefs.getScaling())
              .put("scrolling", prefs.getScrolling());
      return gadgetJson;
    } catch (ProcessingException e) {
      JSONObject errorObj = new JSONObject();
      try {
        errorObj.put("url", context.getUrl())
                .put("moduleId", context.getModuleId());
        errorObj.append("errors", e.getLocalizedMessage());
      } catch (JSONException e1) {
        throw new RpcException(context, e1);
      }
      return errorObj;
    } catch (JSONException e) {
      // Shouldn't be possible
      throw new RpcException(context, e);
    }
  }

  private List<JSONObject> getOrderedEnums(UserPref pref) throws JSONException {
    List<UserPref.EnumValuePair> orderedEnums = pref.getOrderedEnumValues();
    List<JSONObject> jsonEnums = Lists.newArrayListWithExpectedSize(orderedEnums.size());
    for (UserPref.EnumValuePair evp : orderedEnums) {
      JSONObject curEnum = new JSONObject();
      curEnum.put("value", evp.getValue());
      curEnum.put("displayValue", evp.getDisplayValue());
      jsonEnums.add(curEnum);
    }
    return jsonEnums;
  }
}
