package org.apache.shindig.gadgets;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.shindig.common.ContainerConfig;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.common.uri.UriBuilder;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.apache.shindig.gadgets.spec.UserPref;
import org.apache.shindig.gadgets.spec.View;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import static com.google.common.base.MoreObjects.firstNonNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static org.apache.shindig.common.util.HashUtil.checksum;
import static org.apache.shindig.gadgets.spec.View.ContentType.URL;

/**
 * Default url generator. Produces js urls that include checksums for cache-busting.
 * <p>
 * TODO: iframe and js url generation are two distinct things, and should probably be different interfaces.
 */
@Singleton
public class DefaultUrlGenerator implements UrlGenerator {

    static final String IFRAME_URI_PARAM = "gadgets.iframeBaseUri";
    static final String JS_URI_PARAM = "gadgets.jsUriTemplate";

    private static final Pattern ALLOWED_FEATURE_NAME = Pattern.compile("[0-9a-zA-Z\\.\\-]+");
    private static final String OAUTH_GADGET_CALLBACK_URI_PARAM = "gadgets.oauthGadgetCallbackTemplate";

    private final ContainerConfig containerConfig;
    private final GadgetFeatureRegistry registry;
    private final LockedDomainService lockedDomainService;
    private final Map<String, Supplier<Uri>> iframeBaseUris;
    private final Map<String, Supplier<String>> jsUriTemplates;
    private final Map<String, Supplier<String>> oauthCallbackUriTemplates;
    private final Supplier<String> jsChecksum;

    @Inject
    public DefaultUrlGenerator(final ContainerConfig containerConfig,
                               final LockedDomainService lockedDomainService,
                               final GadgetFeatureRegistry registry) {
        this.containerConfig = requireNonNull(containerConfig);
        this.lockedDomainService = requireNonNull(lockedDomainService);
        this.registry = requireNonNull(registry);

        this.iframeBaseUris = new HashMap<>();
        this.jsUriTemplates = new HashMap<>();
        this.oauthCallbackUriTemplates = new HashMap<>();

        initialiseUriSuppliers();
        jsChecksum = this::getJsChecksum;
    }

    private void initialiseUriSuppliers() {
        for (final String container : containerConfig.getContainers()) {
            // Because ContainerConfig#get can end up reading the database, we make all such calls lazy via suppliers
            iframeBaseUris.put(container, () -> Uri.parse(containerConfig.get(container, IFRAME_URI_PARAM)));
            jsUriTemplates.put(container, () -> containerConfig.get(container, JS_URI_PARAM));
            oauthCallbackUriTemplates.put(
                    container, () -> containerConfig.get(container, OAUTH_GADGET_CALLBACK_URI_PARAM));
        }
    }

    private String getJsChecksum() {
        final String js = registry.getAllFeatures().stream()
                .flatMap(feature -> feature.getJsLibraries(null, null).stream())
                .map(JsLibrary::getContent)
                .collect(joining(""));
        return checksum(js.getBytes());
    }

    public String getBundledJsUrl(final Collection<String> features, final GadgetContext context) {
        final Supplier<String> jsPrefix = jsUriTemplates.get(context.getContainer());
        if (jsPrefix == null) {
            return "";
        }
        return jsPrefix.get().replace("%host%", context.getHost())
                .replace("%js%", getBundledJsParam(features, context));
    }

    public String getBundledJsParam(final Collection<String> features, final GadgetContext context) {
        final StringBuilder buf = new StringBuilder();
        boolean first = false;
        for (final String feature : features) {
            if (ALLOWED_FEATURE_NAME.matcher(feature).matches()) {
                if (!first) {
                    first = true;
                } else {
                    buf.append(':');
                }
                buf.append(feature);
            }
        }
        if (!first) {
            buf.append("core");
        }
        buf.append(".js?v=").append(jsChecksum.get())
                .append("&container=").append(context.getContainer())
                .append("&debug=").append(context.getDebug() ? "1" : "0");
        return buf.toString();
    }

    // TODO: This is in need of a rewrite most likely. It doesn't even take locked domain into consideration!
    public String getIframeUrl(final Gadget gadget) {
        final GadgetContext context = gadget.getContext();
        final GadgetSpec spec = gadget.getSpec();
        final String url = context.getUrl().toString();
        final View view = gadget.getCurrentView();
        final View.ContentType type;
        if (view == null) {
            type = View.ContentType.HTML;
        } else {
            type = view.getType();
        }

        final UriBuilder uri;
        switch (type) {
            case URL:
                uri = new UriBuilder(view.getHref());
                break;
            case HTML:
            default:
                // TODO: Locked domain support.
                final Supplier<Uri> iframeBaseUri = iframeBaseUris.get(context.getContainer());
                if (iframeBaseUri != null) {
                    uri = new UriBuilder(iframeBaseUri.get());
                } else {
                    uri = new UriBuilder();
                }
                String host = lockedDomainService.getLockedDomainForGadget(spec, context.getContainer());
                if (host != null) {
                    uri.setAuthority(host);
                }
                break;
        }

        uri.addQueryParameter("container", context.getContainer());
        if (context.getModuleId() != null && !context.getModuleId().equals("0")) {
            uri.addQueryParameter("mid", context.getModuleId());
        }
        if (context.getIgnoreCache()) {
            uri.addQueryParameter("nocache", "1");
        } else {
            uri.addQueryParameter("v", spec.getChecksum());
        }

        uri.addQueryParameter("lang", context.getLocale().getLanguage());
        uri.addQueryParameter("country", context.getLocale().getCountry());
        uri.addQueryParameter("view", context.getView());

        final UserPrefs prefs = context.getUserPrefs();
        for (UserPref pref : gadget.getSpec().getUserPrefs()) {
            final String name = pref.getName();
            final String value = firstNonNull(prefs.getPref(name), pref.getDefaultValue());
            uri.addQueryParameter("up_" + pref.getName(), value);
        }
        // Add URL last, to work around browser bugs
        if (!type.equals(URL)) {
            uri.addQueryParameter("url", url);
        }

        return uri.toString();
    }

    public String getGadgetDomainOAuthCallback(final String container, final String gadgetHost) {
        final Supplier<String> callback = oauthCallbackUriTemplates.get(container);
        if (callback == null) {
            return null;
        }
        return callback.get().replace("%host%", gadgetHost);
    }
}
