package org.apache.shindig.gadgets.variables;

import com.google.common.collect.ImmutableMap;
import org.apache.shindig.common.uri.Uri;
import org.apache.shindig.gadgets.UserPrefs;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.apache.shindig.gadgets.variables.Substitutions.Type;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class UserPrefSubstituterTest {

    private final static String DEFAULT_NAME = "default";
    private final static String DEFAULT_VALUE = "default value";
    private final static String USER_NAME = "user";
    private final static String USER_VALUE = "user value";
    private final static String OVERRIDE_NAME = "override";
    private final static String OVERRIDE_VALUE = "override value";
    private final static String UNESCAPED_USER_VALUE = "<hello, & world > \"";
    private final static String ESCAPED_USER_VALUE = "&lt;hello, &amp; world &gt; &quot;";
    private static final String DEFAULT_XML
            = "<Module>" +
            "<ModulePrefs title=\"Hello, __UP_world__\"/>" +
            "<UserPref name=\"" + DEFAULT_NAME + "\" datatype=\"string\"" +
            " default_value=\"" + DEFAULT_VALUE + "\"/>" +
            "<UserPref name=\"" + USER_NAME + "\" datatype=\"string\"/>" +
            "<UserPref name=\"" + OVERRIDE_NAME + "\" datatype=\"string\"" +
            "  default_value=\"FOOOOOOOOOOBAR!\"/>" +
            "<Content type=\"html\"/>" +
            "</Module>";

    private final Substitutions substituter = new Substitutions();

    private GadgetSpec spec;

    @Before
    public void setUp() throws Exception {
        spec = new GadgetSpec(Uri.parse("#"), DEFAULT_XML);
    }

    @Test
    public void testSubstitutions() {
        Map<String, String> map = ImmutableMap.of(USER_NAME, USER_VALUE, OVERRIDE_NAME, OVERRIDE_VALUE);
        UserPrefs prefs = new UserPrefs(map);
        UserPrefSubstituter.addSubstitutions(substituter, spec, prefs);

        assertEquals(DEFAULT_VALUE,
                substituter.getSubstitution(Type.USER_PREF, DEFAULT_NAME));
        assertEquals(USER_VALUE,
                substituter.getSubstitution(Type.USER_PREF, USER_NAME));
        assertEquals(OVERRIDE_VALUE,
                substituter.getSubstitution(Type.USER_PREF, OVERRIDE_NAME));
    }

    @Test
    public void testEscaping() {
        Map<String, String> map = ImmutableMap.of(USER_NAME, UNESCAPED_USER_VALUE);
        UserPrefs prefs = new UserPrefs(map);
        UserPrefSubstituter.addSubstitutions(substituter, spec, prefs);
        assertEquals(ESCAPED_USER_VALUE,
                substituter.getSubstitution(Type.USER_PREF, USER_NAME));
    }
}
